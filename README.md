# jhw/opam-personal - OPAM Repository for Personal Development

This OPAM repository contains package descriptions for development versions of
my personal open source projects.

## Copyright

Copyright (C) 2019, james woodyatt [< jhw@conjury.org >](email:jhw@conjury.org).  
All rights reserved.

## License

All the texts and other data comprising this repository are licensed under the
[CC0 1.0 Universal](http://creativecommons.org/publicdomain/zero/1.0/) license.
